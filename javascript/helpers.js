export const _ = {
	is(str,obj) {
		const type = Object.prototype.toString.call(obj).slice(8, -1).toLocaleLowerCase()

		if (str === 'number' && type === 'number') return obj === obj
		if (str === 'NaN') return Number.isNaN(obj)

		if (str === 'nill' && (type === 'null' || type === 'undefined')) return true

		return str === type
	},
	dropFirst(arr) {
		return arr.slice(1)
	},
	dropLast(arr) {
		return arr.slice(0,-1)
	},
	filter(fn) {
		return (arr)=> arr.filter(fn)
	},
	append(value) {
		return (arr)=> [...arr, value]
	},
	prepend(value) {
		return (arr)=> [value, ...arr]
	},
	insert(value, position) {
		return (arr)=> [...arr.slice(0,position), value, ...arr.slice(position,arr.length)]
	}
}

export function $(s,c) {
	// debugger
	class S {
		constructor(s,c) {
			this._ = []

			if (!s) return this

			if (s instanceof HTMLElement) {
				this._ = [s]
				return this
			}
			if (s instanceof NodeList) {
				this._ = [...s]
				return this
			}

			let ctx = document 
			if (c && c instanceof HTMLElement) ctx = c
			if (c && c instanceof NodeList) ctx = c[0]
			if (c && c._ && c._[0] instanceof HTMLElement) ctx = c._[0]
			
			if (s[0] === '#') {
				const e = ctx.getElementById(s.substring(1))
				if (e) this._ = [e]
			}
			else if (s[0] === '+') {
				const e = ctx.querySelectorAll(`[data-s="${s.substring(1)}"]`)
				if (e.length) this._ = [...e]
			}
			else {
				const e = ctx.querySelectorAll(s)
				if (e.length) this._ = [...e]
			}

			return this
		}
		get length() {
			return this._.length
		}
		get elm() {
			return this._[0]
		}
		each(cb) {
			this._.forEach(cb)
			return this
		}
		find(s) {
			return new S(s, this._[0])
		}
		closest(s) {
			return new S(this._[0].closest(s))
		}
		on(ev, cb, opt) {
			this.each((node)=> { node.addEventListener(ev, cb, opt)})
			return this
		}
	}

	return new S(s,c)
}

export function pop(text, type='info', autoclose = true) {
	let $c = document.getElementById('_pop_container_')

	if (!$c) {
		$c = document.createElement('div')
		$c.id = '_pop_container_'
		$c.setAttribute('style',[
			'position: fixed',
			'bottom: 15px',
			'right: 15px',
			'z-index: 10000',
			'',
		].join(';'))

		document.body.appendChild($c)
	}

	function createPop() {
		let sto = null
		const typeColor = type==='success' ? '#31cb77' : (type==='error'? '#e54167' : '#0bb4e7')

		const p = document.createElement('div')
		p.setAttribute('style',[
			'display: flex',
			'width: 340px',
			'margin: 10px',
			'padding: 10px 15px',
			'color: #fff',
			`border-left: 10px solid ${typeColor}`,
			'background-color: #3f6171',
		].join(';'))

		p.innerHTML = `
			<span>${text}</span>
			<span style="margin-left: auto; cursor:pointer">&times;</span>
		`

		if (autoclose) {
			sto = setTimeout(() => {
				p.remove()
				clearTimeout(sto)
				sto = null
			}, 5000);
		}

		p.addEventListener('click', ()=> {
			p.remove()
			if (sto) clearTimeout(sto)
		})

		return p
	}

	const p = createPop()
	$c.appendChild(p)

	return p
}

export const request = (function request() {
	function request(url, opt = {}, type = '') {
		if (!type || !url) return

		const defaults = { method: type }
	
		if (opt.body) defaults.body = JSON.stringify(opt.body)
		if (type !== 'GET') defaults.headers = { 'Content-type': 'application/json; charset=UTF-8', }
	
		const options = Object.assign({}, opt, defaults)
	
		return fetch(url, options).then((resp) => {
			if (!resp.ok) throw (`${resp.status} - ${resp.statusText}`);
			return resp.json()
		})
	}

	const httpMethods = ['get', 'post', 'patch', 'put', 'delete']
	const methods = httpMethods.reduce(function(acc, name) {
		acc[name] = (url, opt)=> request(url, opt, name.toUpperCase())
		return acc
	}, {})

	return methods
}())
