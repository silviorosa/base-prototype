import { _, request } from '../helpers.js'

const test = window.tape

const vivid = 'https://vivid-vein.glitch.me/api/query?success=1&info=Lorem'
const typicode = 'https://jsonplaceholder.typicode.com/'
const phpApi = '../../php/app/server-api/' 
const urlRequest = phpApi

document.body.insertAdjacentHTML('beforeend', `
	<div class="container" style="opacity:0; pointer-events:none">
		<form id="form-1">
			<input type="text" name="name" value="John Doe">
			<input type="text" name="email" value="John.Doe@example.com">
		</form>
	</div>
`)


test('request', (t)=> {
	t.ok(request, 'exists')
	t.ok( _.is('object', request), "is object")
	t.ok( _.is('undefined', request.get()), "Method request.get()")
	t.ok( _.is('undefined', request.post()), "Method request.post()")
	t.ok( _.is('undefined', request.put()), "Method request.put()")
	t.ok( _.is('undefined', request.patch()), "Method request.patch()")
	t.ok( _.is('undefined', request.delete()), "Method request.delete()")
	t.end()
})

test('request.get(url)', (t)=> {

	t.plan(3)
	const req = request.get(urlRequest)

	t.ok( _.is('promise', req), "returns promise")

	req.then(function(resp) {
		t.ok( _.is('object', resp), "response")
	})
	
	request.get('bad/url').catch((err)=> {
		t.ok(err, 'Catch error: '+ err)
	})
})

test('request.post(url, opt)', (t)=> {

	t.plan(2)

	const body = { title: 'foo'}
	const req = request.post(urlRequest, { body })

	t.ok( _.is('promise', req), "returns promise")

	req.then(function(resp) {
		t.ok( _.is('object', resp), "response")
	})
})

test('request.patch(url, opt)', (t)=> {

	t.plan(2)

	const body = { title: 'foo'}
	const reqTodos = request.patch(urlRequest, { body })

	t.ok( _.is('promise', reqTodos), "returns promise")

	reqTodos.then(function(resp) {
		t.ok( _.is('object', resp), "response")
	})
})

test('request.put(url, opt)', (t)=> {

	t.plan(2)

	const body = { title: 'foo'}
	const req = request.put(urlRequest, { body })

	t.ok( _.is('promise', req), "returns promise")

	req.then(function(resp) {
		t.ok( _.is('object', resp), "response")
	})
})

test('request.delete(url)', (t)=> {

	t.plan(2)

	const req = request.delete(urlRequest)

	t.ok( _.is('promise', req), "returns promise")

	req.then(function(resp) {
		t.ok( _.is('object', resp), "response")
	})
})

test('Post FormData', (t)=> {
	t.plan(1)
	
	const $form = document.getElementById('form-1')
	const formData = new FormData($form)
	formData.append('host', window.location.host)

	const body = Object.fromEntries(formData.entries())
	request.post(urlRequest, { body }).then((resp)=> {
		t.ok(_.is('object', resp), 'Posted')
	})
})