import { $, _ } from '../helpers.js'

const test = window.tape

document.body.insertAdjacentHTML('beforeend', `
	<div class="container" id="container">
		<div class="div-A div-A1">
			<button data-s="btnMultiple" type="button"></button>
			<button data-s="btnMultiple" type="button"></button>
		</div>

		<div class="div-A div-A2">
			<button data-s="btnMultiple" type="button"></button>
			<a data-s="btnUnique" href="#0"></a>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
`)

test('Function $', (t)=> { 

	t.ok($,'$ Exist')
	t.ok( typeof $ === 'function', '$ is function')

	t.ok( _.is('object', $()),         "$() returns object")
	t.ok( _.is('object', $('')),       "$('') returns object")
	t.ok( _.is('object', $('body')),   "$('body') returns object")
	t.ok( _.is('object', $('unkown')), "$('unkown') returns object")
	t.end() 
})

test('Selector $(selector)', (t)=> { 
	t.ok( $('div').elm instanceof HTMLElement,  "$('div') by tag")
	t.ok( $('#container').elm instanceof HTMLElement,  "$('#container') by id")
	t.ok( $('.div-A').elm instanceof HTMLElement,  "$('.div-A') by class")
	t.ok( $('+btnMultiple').elm instanceof HTMLElement,  "$('+btnMultiple') by data-se=\"btnMultiple\" ")
	t.end() 
})

test('Context $(s, context)', (t)=> { 
	t.ok( $('body',  $('html')).elm === document.body,  "$('body',  $('html')).elm is document.body")
	t.ok( $('+btnMultiple').length === 3,               "$('+btnMultiple').length is 3")
	t.ok( $('+btnMultiple', $('.div-A2')).length === 1, "$('+btnMultiple', $('.div-A2')).length === 1 is 1")
	t.end() 
})

test('$ property (private) ._ | Array of nodes', (t)=> {
	t.ok( _.is('array', $()._),         "$()._ is Array")
	t.ok( _.is('array', $('')._),       "$('')._ is Array")
	t.ok( _.is('array', $('body')._),   "$('body')._ is Array")
	t.ok( _.is('array', $('unkown')._), "$('unkown')._ is Array")

	t.ok( $()._.length === 0,         "$()._.length is 0")
	t.ok( $('')._.length === 0,       "$('')._.length is 0")
	t.ok( $('body')._.length === 1,   "$('body')._.length is 1")
	t.ok( $('unkown')._.length === 0, "$('unkown')._.length is 0")
	
	t.end() 
})

test('$ property .length | Length of ._', (t)=> {
	t.ok( $().length === 0,         "$().elm is 0")
	t.ok( $('').length === 0,       "$('').elm is 0")
	t.ok( $('body').length === 1,   "$('body').elm is 1")
	t.ok( $('unkown').length === 0, "$('body').elm is 0")
	t.end() 
})

test('$ property .elm | First element in ._', (t)=> {
	t.ok( $().elm === undefined,           "$().elm is undefined")
	t.ok( $('').elm === undefined,         "$('').elm is undefined")
	t.ok( $('body').elm === document.body, "$('body').elm is body node element")
	t.ok( $('unkown').elm === undefined,   "$('body').elm is undefined")
	t.end() 
})

test('$ method .each | Loop in ._', (t)=> {
	const $base = $('#container').elm
	
	$('div', $base).each((elm)=> { elm.classList.add('test-each') })

	t.ok( $('div', $base).length === $('.test-each', $base).length, 'Add class to all divs ' )

	$('div', $base).each((elm)=> { elm.classList.remove('test-each') })
	t.end() 
})

test('$ method .find', (t)=> {
	const $base = $('#container')
	t.ok( $base.find('span').length === 3, 'find span' )
	t.end() 
})

test('$ method .closest', (t)=> {
	const $base = $('+btnUnique')
	t.ok( $base.closest('.div-A').length === 1, 'Closest div-A' )
	t.end() 
})

test('$ method .on | Events', (t)=> {
	const $base = $('#container').elm
	
	$('+btnUnique', $base).on('click', (ev)=> { ev.target.classList.add('test-event') })
	$('+btnUnique', $base).elm.click()
	t.ok($('+btnUnique', $base).elm.classList.contains('test-event'), 'Add class by click')
	$('+btnUnique', $base).elm.classList.remove('test-event')


	$('+btnMultiple', $base).on('click', (ev)=> { 
		$('span', $base).each(elm=> elm.classList.add('test-event-2'))
	 })
	$('+btnMultiple', $base).elm.click()
	t.ok($('span', $base).length === $('.test-event-2', $base).length, 'Add class by click in multiple elements')
	$('span', $base).each(elm=> elm.classList.remove('test-event-2'))
	
	t.end() 
})

test('Chain methods', (t)=> {
	$('+btnMultiple')
	.each(elm=> elm.classList.add('test-chain-btn'))
	.closest('.container')
	.each(elm=> elm.classList.add('test-chain-container'))
	.find('+btnUnique')
	.each(elm=> elm.classList.add('test-chain-btn-unique'))

	t.ok($('+btnMultiple').elm.classList.contains('test-chain-btn'),      "Class test-chain-btn added")
	t.ok($('#container').elm.classList.contains('test-chain-container'),  "Class test-chain-container added")
	t.ok($('+btnUnique').elm.classList.contains('test-chain-btn-unique'), "Class test-chain-btn-unique added")

	$('+btnMultiple')
	.each(elm=> elm.classList.remove('test-chain-btn'))
	.closest('.container')
	.each(elm=> elm.classList.remove('test-chain-container'))
	.find('+btnUnique')
	.each(elm=> elm.classList.remove('test-chain-btn-unique'))

	t.end()
})
