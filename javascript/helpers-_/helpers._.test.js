import { _ } from '../helpers.js'

const test = window.tape

test('_.is', (t)=> {
	t.ok( _.is('number', 2), "Number" )
	t.notOk( _.is('number', 2 * 'two'), "NaN is not number ...:(" )
	t.ok( _.is('NaN', 2 * 'two'), "NaN" )
	t.ok( _.is('string', 'string'), "String" )
	t.ok( _.is('boolean', false), "Boolean" )
	t.ok( _.is('array', []), "Array" )
	t.ok( _.is('object', {}), "Object" )
	t.ok( _.is('function', ()=> {}), "Function" )
	t.ok( _.is('null', null), "null" )
	t.ok( _.is('undefined', undefined), "undefined" )
	t.ok( _.is('nill', null), "nill > null" )
	t.ok( _.is('nill', undefined), "nill > undefined" )
	t.ok( _.is('symbol', Symbol()), "Symbol" )
	t.ok( _.is('set', new Set()), "Set" )
	t.ok( _.is('map', new Map()), "Map" )
	t.ok( _.is('promise', new Promise(()=> {})), "Promise" )
	t.ok( _.is('promise', fetch('')), "Promise fetch" )
	t.end()
})

test('_.dropFirst', (t)=> {
	const arr = [1,2,3,4,5]
	t.ok( _.dropFirst(arr)[0] === 2)
	t.ok( arr[0] === 1)
	t.end()
})

test('_.dropLast', (t)=> {
	const arr = [1,2,3,4,5]
	const d = _.dropLast(arr)
	t.ok( d[d.length - 1] === 4)
	t.ok( arr[arr.length - 1] === 5)
	t.end()
})

test('_.filter', (t)=> {
	const arr = [1,2,3,4,5]
	const d = _.filter((v)=> v !== 2)(arr)
	t.ok( d[1] === 3)
	t.ok( arr[1] === 2)

	const arr2 = [
		{ a:1, b:2, c:3},
		{ a:11, b:22, c:33},
		{ a:111, b:222, c:333},
	]
	const o = _.filter((v)=> v.a !== 11)(arr2)
	t.ok( o[1].a === 111)
	t.ok( arr2[1].a === 11)
	t.end()
})

test('_.append', (t)=> {
	const arr = [1,2,3,4,5]
	const d = _.append(0)(arr)
	t.ok( d[d.length - 1] === 0)
	t.ok( arr[arr.length - 1] === 5)
	t.end()
})

test('_.prepend', (t)=> {
	const arr = [1,2,3,4,5]
	const d = _.prepend(0)(arr)
	t.ok( d[0] === 0)
	t.ok( arr[0] === 1)
	t.end()
})

test('_.insert', (t)=> {
	const arr = [1,2,3,4,5]
	const d = _.insert(0,2)(arr)
	t.ok( d[2] === 0)
	t.ok( arr[2] === 3)
	t.end()
})

