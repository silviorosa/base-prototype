// import { html, render } from 'https://unpkg.com/lit-html@2.0.0/lit-html.js'
// const myTemplate = (name) => html`<p>Hello ${name}</p>`
// render(myTemplate('World'), document.body)

// const r = R.pipe( R.reduce(R.add, 0), R.toString, R.concat('Value: '))
// console.log(r([1,2,3]))

import { $, pop, request } from '../../javascript/helpers.js'

pop('1 Lorem')
pop('2 Lorem', null)
pop('3 Lorem', 'success')
pop('4 Lorem', 'error', false)

const $form = $('#form')

$('+btnForm', $form).on('click', function(ev) {
	this.disabled = true
	$form.elm.style.outline = '5px solid #fee'
	$('option:nth-child(3)', $('+selectOption').elm).elm.selected = true
	$('input', $form).each(elm=> elm.style.outline = '5px solid #efe')
	$('input[type="checkbox"]', $form).elm.checked = true
	$form.elm.insertAdjacentHTML('beforebegin', '<p style="color:red">js changed this</p>')
})

if ($('#code-fibonacci').length) {
	$('#code-fibonacci').elm.innerHTML = [
		'function fibonacci(num) {',
		'	if(num < 2) return num',
		'	return fibonacci(num - 1) + fibonacci(num - 2)',
		'}',
	].join('\n')
}

tape('Test names', (t)=> { t.ok(true,'Is true'); t.end() })

request.get('app/server-api/').then((resp)=> {
	console.log(resp)
}) 

request.post('app/server-api/', { body: {someKey: 'Lorem', otherKey: true}}).then((resp)=> {
	console.log(resp)
})

request.patch('app/server-api/').then((resp)=> {
	console.log(resp)
}) 

request.put('app/server-api/', { body: {someKey: 'Lorem', otherKey: true}}).then((resp)=> {
	console.log(resp)
})

request.delete('app/server-api/').then((resp)=> {
	console.log(resp)
}) 
