<?php 

require_once(__DIR__.'/app/configs.php');
require_once(__DIR__.'/app/libs/BramusRouter.php');

//https://github.com/bramus/router
$router = new \Bramus\Router\Router();

$router->set404(function() {
	header('HTTP/1.1 404 Not Found');
	echo '404 Not Found';
});

$router->get('/', function() use ($H) {
	include(__DIR__.'/views/home.php');
});

foreach ($H->viewsNames as $view) {
	$router->get('/'. $view, function() use ($H, $view) {
		include(__DIR__.'/views/'.$view.'.php');
	});
}

$router->run();