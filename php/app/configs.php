<?php 

include_once(__DIR__.'/libs/LoremIpsum.php');


class Helper  {
	public $name = 'php prototype';
	
	public function __construct() {
		$this->lipsum = new joshtronic\LoremIpsum();
		$this->viewsNames = $this->viewsFiles();
		$this->baseUrl = $this->createBaseUrl() .'/';
	}

	public function lorem($size=null) {
			$titleSize = $size ? $size : [3,5][rand(0,1)];
			return ucFirst($this->lipsum->words($titleSize));
	}

	public function loremText($size=1) {
			return $this->lipsum->sentences($size);
	}

	private function viewsFiles() {
		$path    = __DIR__.'/../views';
		$files = scandir($path);
		$files = array_diff(scandir($path), array('.', '..'));
		$filesNames = [];
		foreach ($files as $file) {
			if ($file !== 'home.php' && is_file(__DIR__.'/../views/'. $file)) {
				$filesNames[] = basename($file, '.php');
			}
		}
		return $filesNames;
	}

	private function createBaseUrl() {
		$dirSlash = str_replace('\\','/', dirname(dirname(__DIR__)) );
		$intersect = array_intersect( explode('/', $dirSlash), explode('/', dirname($_SERVER['REQUEST_URI'])) );
		return '/'. ltrim(join('/', $intersect), '/');
	}
}

$H = new Helper();
