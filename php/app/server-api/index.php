<?php 

require_once(__DIR__.'/../configs.php');
require_once(__DIR__.'/../libs/BramusRouter.php');


//https://github.com/bramus/router
$router = new \Bramus\Router\Router();

function resp($data) {
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode([ 'success'=> 1, 'info'=> $_SERVER['REQUEST_METHOD'] .' Server Api', '_get'=> $data]);
}

$router->get('/', function() {
	resp($_GET);
});

$router->post('/', function() {
	$body = json_decode(file_get_contents('php://input'), true);
	resp($body);
});

$router->patch('/', function() {
	$body = json_decode(file_get_contents('php://input'), true);
	resp($body);
});

$router->put('/', function() {
	$body = json_decode(file_get_contents('php://input'), true);
	resp($body);
});

$router->delete('/', function() {
	$body = json_decode(file_get_contents('php://input'), true);
	resp($body);
});

$router->run();