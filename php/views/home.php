<?php include_once(__DIR__.'/includes/header-1.php') ?>
<?php include_once(__DIR__.'/includes/header-2.php') ?>

<div class="container">
	<h1><?= $H->lorem() ?></h1>
	<p></p>
	<div class="mb">
		<h1>h1 <?= $H->lorem() ?></h1>
		<h2>h2 <?= $H->lorem() ?></h2>
		<h3>h3 <?= $H->lorem() ?></h3>
		<p>Paragraph Lorem ipsum dolor sit <strong>amet</strong> <?= $H->loremText() ?></p>
		<div class="mb">
			<a href="#0">Link</a>
			<button type="button">Button</button> <code>code {}</code>
		</div>
	</div>

	<form class="mb-xl" id="form" data-s="form1">
		<div class="row">
			<div class="col col-sm-6">
				<label class="mb"> Lorem
					<input class="form-control" data-s="inputText" placeholder="Lorem" type="text">
				</label>

				<select class="form-control mb" data-s="selectOption">
					<option value="">---</option>
					<option value="1">Option 1</option>
					<option value="2">Option 2</option>
					<option value="3">Option 3</option>
				</select>
			</div>

			<div class="col col-sm-6">
				<label class="mb">
					<input type="checkbox"> checkbox
				</label>
				<label class="mb">
					<input type="radio"> radio
				</label>
			</div>
		</div>

		<button class="btn" data-s="btnForm" type="button">Button</button>
		<a class="btn" href="#0">Link</a>
	</form>

	<hr class="mb-xl" data-s="uiui">

	<div class="mb-xl">
		<img src="https://via.placeholder.com/640x320" width="640" height="320" alt="">
	</div>

	<ul>
		<li>List 1</li>
		<li>List 2</li>
		<li>
			List 3
			<ul>
				<li>List 3.1</li>
				<li>List 3.2</li>
				<li>
					List 3.3
					<ul>
						<li>List 3.1</li>
						<li>List 3.2</li>
						<li>List 3.3</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>

	<pre><code id="code-fibonacci"></code></pre>
</div>

<?php include_once(__DIR__.'/includes/footer.php') ?>