<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $H->name ?></title>

	<!-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"> -->
	<!-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script> -->

	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.1/css/bootstrap.min.css"> -->
	<!-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.1/js/bootstrap.bundle.min.js"></script> -->


	<!-- <script defer src="https://cdnjs.cloudflare.com/ajax/libs/ramda/0.27.1/ramda.min.js"></script> -->

	<link rel="stylesheet" href="<?= $H->baseUrl ?>css/styles.css">

	<script defer src="<?= $H->baseUrl ?>javascript/vendor/tape.min.js "></script>

	<script type="module" src="<?= $H->baseUrl ?>javascript/helpers.js"></script>
	<script type="module" src="<?= $H->baseUrl ?>php/assets/scripts.js"></script>
</head>
<body>
	<header>
		<div class="container pt-sm pb-sm mb-xl d-f ai-c">
			<strong class="d-ib co-gray mr-auto"><?= $H->name ?></strong>
			<a class="p-sm" href="../">root</a>
			<a class="p-sm" href="<?= $H->baseUrl ?>php/">home</a>
			<?php foreach ($H->viewsNames as $view) : ?>
				<a class="p-sm" href="<?= $H->baseUrl .'php/'. $view ?>"><?= $view ?></a>
			<?php endforeach ?>
		</div>
	</header>